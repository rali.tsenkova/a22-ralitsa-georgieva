
import com.telerikacademy.MyArrayList;
import com.telerikacademy.MyList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MyArrayListTests {

    private MyList<Integer> list;

    @BeforeEach
    public void init() {
        //Arrange
        list = new MyArrayList<>();
    }

    @Test
    public void get_should_returnRightElement_when_IndexIsValid() {
        // Arrange
        list.add(1);
        list.add(2);
        // Act
        Integer result = list.get(0);

        // Assert
        Assertions.assertEquals(1, result);
    }

    @Test
    public void get_should_throw_when_indexIsBelowZero() {
        // Arrange
        // Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> list.get(-1));
    }

    @Test
    public void get_should_throw_when_indexIsAboveTheUpperBound() {
        // Arrange
        // Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> list.get(0));

    }

//    @Test //Does not catch the bug!
//    public void get_should_throw_when_indexIsAboveTheUpperBoundPlusOne() {
//        // Arrange
//        // Act Assert
//        Assertions.assertThrows(IndexOutOfBoundsException.class,
//                () -> list.get(1));
//
//    }

    @Test
    public void set_should_setRightElement_when_indexIsValid() {
        // Arrange
        list.add(1);
        list.add(2);
        list.set(0, 10);
        // Act
        Integer result = list.get(0);
        // Assert
        Assertions.assertEquals(10, result);
    }

    @Test
    public void set_should_throw_when_indexIsBelowZero(){
        // Arrange
//        list.add(1);
//        list.add(2);
        // Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                ()-> list.set(-1,10));
    }

    @Test
    public void set_should_throw_when_indexIsAboveUpperBound(){
        // Arrange
//        list.add(1);
//        list.add(2);
        // Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                ()-> list.set(0,10));
    }

    @Test
    public void constructor2_should_createNewArrayList_when_validValuesArePassed(){
        MyList<String> listString = new MyArrayList<>(new String []{"alfa", "beta", "gama"});
    }

    @Test
    public void getLast_should_getLastElement_when_positionIsValid(){
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        Integer result = list.getLast();

        Assertions.assertEquals(4, result);
    }

    @Test
    public void getFirst_should_getFirstElement_when_positionIsValid(){
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        Integer result = list.getFirst();

        Assertions.assertEquals(1, result);
    }

    @Test
    public void getSize_should_returnSize_when_listLengthIsMoreThanZero() {
        Assertions.assertEquals(4, list.getSize());

    }

    @Test
    public void add_should_doubleLength_when_addingMoreThanFourElements(){
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        Assertions.assertEquals(8, list.getSize());
    }

    @Test
    public void findIndexOf_should_returnValidIndex_when_validValueIsPassed(){
        list.add(1);
        list.add(2);
        list.add(3);
        Assertions.assertEquals(0, list.findIndexOf(1));
    }

    @Test
    public void findIndexOf_should_returnInvalidIndex_when_notValidValueIsPassed(){
        list.add(1);
        list.add(2);
        list.add(3);
        Assertions.assertEquals(-1, list.findIndexOf(10));
    }

}
