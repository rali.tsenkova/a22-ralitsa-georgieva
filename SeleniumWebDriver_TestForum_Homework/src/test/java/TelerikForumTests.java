
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TelerikForumTests {
    public static final String telerikForumUrl = "https://stage-forum.telerikacademy.com";
    private WebDriver driver;


    @BeforeClass //begins when the class starts (@Before begins before every test)
    public static void classInit() {
        System.setProperty("webdriver.chrome.driver", "D:\\Rali\\TelerikAcademy\\Installs\\chromedriver_win32\\chromedriver.exe");
        //System.setProperty("webdriver.gecko.driver", "D:\\Rali\\TelerikAcademy\\Installs\\geckodriver-v0.27.0-win64\\geckodriver.exe");
    }

    @Before
    public void testInit() {
        driver = new ChromeDriver();
        //driver = new FirefoxDriver();
        driver.get(telerikForumUrl);

    }

    @After
    public void testCleanup(){
        driver.close();
    }

    @Test
    public void logIn_LogOut() {
        logIn(driver);
        logOut(driver);
    }

    @Test
    public void createTopic_When_LogIn(){
        String topicName = "Little star";
        String topicText = "Lorem ipsum dolor sit Lorem ipsum dolor sit";
        String expectedResult = "little-star";

        logIn(driver);
        waitTime(3000);

        WebElement buttonCreateTopic = driver.findElement(By.xpath("//button[@id='create-topic']/span"));
        buttonCreateTopic.click();
        waitTime(2000);

        WebElement inputTitle = driver.findElement(By.xpath("//input[@aria-label='Type title, or paste a link here']"));
        inputTitle.sendKeys(topicName);
        waitTime(2000);

        WebElement inputText = driver.findElement(By.xpath("//textarea[@class='d-editor-input ember-text-area ember-view']"));
        inputText.click();
        inputText.sendKeys(topicText);
        waitTime(2000);

        WebElement buttonCreate = driver.findElement(By.xpath("//button[@class='btn btn-icon-text btn-primary create ember-view']"));
        buttonCreate.click();
        waitTime(2000);

        Assert.assertTrue(driver.getCurrentUrl().contains(expectedResult));

        logOut(driver);
    }

    public void logIn(WebDriver driver) {
        WebElement buttonLogIn = driver.findElement(By.xpath("//button[@class='widget-button btn btn-primary btn-small login-button btn-icon-text']"));
        buttonLogIn.click();

        WebElement fieldEmail = driver.findElement(By.xpath("//input[@name='Email']"));
        fieldEmail.click();
        fieldEmail.sendKeys("ralka_17@abv.bg");

        WebElement fieldPass = driver.findElement(By.xpath("//input[@name='Password']"));
        fieldPass.click();
        fieldPass.sendKeys("testP@ss");

        WebElement buttonSignIn = driver.findElement(By.xpath("//button[@id='next']"));
        buttonSignIn.click();
    }

    public void logOut(WebDriver driver) {
        WebElement buttonAvatar = driver.findElement(By.xpath("//img[@class='avatar']"));
        buttonAvatar.click();

        WebElement linkActiveUser = driver.findElement(By.xpath("//a[@class='widget-link user-activity-link']"));
        linkActiveUser.click();

        WebElement linkLogOut = driver.findElement(By.xpath("//li[@class='logout read']"));
        linkLogOut.click();
    }

    private void waitTime(long timeoutMilliseconds) {
        try {
            Thread.sleep(timeoutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
