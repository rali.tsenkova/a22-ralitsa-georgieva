package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserActions {
	public WebDriver getDriver() {
		return driver;
	}

	final WebDriver driver;

	public UserActions() {
		this.driver = Utils.getWebDriver();
	}

	public static void loadBrowser() {
		Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
	}

	public static void quitDriver(){
		Utils.tearDownWebDriver();
	}

	public void clickElement(String key){
		Utils.LOG.info("Clicking on element " + key);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		element.click();
	}

	public void typeValueInField(String value, String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(value);
	}

	public void agreeWithConsent() {
		WebElement iFrame = driver.findElement(By.xpath("//div[@id='cnsw']//iframe"));
		driver.switchTo().frame(iFrame);
		WebElement agreeButton = driver.findElement(By.id("introAgreeButton"));
		agreeButton.click();
		driver.switchTo().defaultContent();
	}

	//############# WAITS #########

	public void waitForElementVisible(String locator, int seconds) {
		WebElement element= driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
		}
		catch (Exception exception){
			Assert.fail("Element with locator: '" + locator + "' was not found.");
		}
	}

	//############# ASSERTS #########

	public void assertElementPresent(String locator){
		Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
	}

	public void assertNavigatedUrl(String urlKey){
		String currentUrl = driver.getCurrentUrl();
		String expectedUrl = Utils.getConfigPropertyByKey(urlKey);
		Assert.assertTrue( "Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + expectedUrl, currentUrl.contains(expectedUrl));
	}
}
