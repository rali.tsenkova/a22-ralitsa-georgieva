Mata:
@qa

Narrative:
As a future QA
I want to find info about Alpha QA Academy
So that I can join the course

Scenario: Find QA Academy in Telerik Academy website
When Click academy.Cookies element
And Click academy.AlphaAnchor element
And Click academy.QaGetReadyLink element
Then Click academy.SignUpNavButton element