package testCases;

import org.junit.Test;

import java.sql.Timestamp;

public class TelerikForumTest extends BaseTest {
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    private final String TOPIC_NAME = "Little star"+timestamp;
    private final String TOPIC_CONTENT = "Lorem ipsum dolor sit"+timestamp;

    @Test
    public void login_when_validCredentialsArePassed(){
        actions.loginForum();
        actions.waitForElementVisible("forum.currentUser", 10);

        actions.assertElementPresent("forum.currentUser");
        actions.logoutForum();
    }

    @Test
    public void createTopic_when_loginIsValid(){
        actions.loginForum();
        actions.waitForElementVisible("forum.buttonCreateTopic", 10);
        actions.clickElement("forum.buttonCreateTopic");
        actions.waitForElementVisible("forum.inputTitleField", 10);
        actions.typeValueInField(TOPIC_NAME, "forum.inputTitleField");
        actions.waitForElementVisible("forum.inputTextField", 10);
        actions.typeValueInField(TOPIC_CONTENT, "forum.inputTextField");
        actions.waitForElementVisible("forum.buttonCreate", 10);
        actions.clickElement("forum.buttonCreate");
        actions.assertNavigatedUrl("forum.expectedUrl");
    }
}
