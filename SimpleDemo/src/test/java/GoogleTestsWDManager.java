import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class GoogleTestsWDManager {
    public static final String googleSearchUrl = "https://google.com";
    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeClass
    public static void classInit(){
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void testInit(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");

//        this does not use UI but here doesn't work ... some problem  with the iframe !?
//        options.addArguments("headless");
//        options.addArguments("window-size=1920,1080");

        driver = new ChromeDriver(options);

// DIFFERENT WAYS FOR WAITING
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);

        wait = new WebDriverWait(driver, 10);

        driver.get(googleSearchUrl);
        agreeWithConsent(driver);
    }

    @After
    public void testCleanup(){
        driver.close();
    }

    @Test
    public void googleHomePageNavigated_When_GoogleOpened(){
        String expectedHomePageUrl = "https://www.google.com/";
        Assert.assertEquals("The page was not navigated", driver.getCurrentUrl(), expectedHomePageUrl);
    }

    @Test
    public void googleSearchResultsPageNavigated_When_SearchIsPerformed(){

        WebElement searchTextBox=driver.findElement(By.xpath("//input[@name='q']"));
        searchTextBox.sendKeys("Telerik Academy Alpha");      // + Keys.ENTER);

        WebElement suggestionSection = driver.findElement(By.className("UUbT9"));
        WebElement searchButton = suggestionSection.findElement(By.name("btnK"));

//        wait.until(ExpectedConditions.elementToBeClickable(searchButton));

        searchButton.click();
//        new Actions(driver).moveToElement(searchButton).click().perform();


        // Assert page navigated
        Assert.assertTrue("Page was not navigated. Actual url: " + driver.getCurrentUrl() +
                        ". Expected url: " + "https://www.google.com/search",
                driver.getCurrentUrl().contains("https://www.google.com/search"));
    }

//  NOT APPROPRIATE METHOD FOR WAITING
//    private void waiting(long timeoutMilliseconds) {
//        try {
//            Thread.sleep(timeoutMilliseconds);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }

    private void agreeWithConsent(WebDriver driver) {
        WebElement iFrame = driver.findElement(By.xpath("//div[@id='cnsw']//iframe"));
        driver.switchTo().frame(iFrame);
        WebElement agreeButton = driver.findElement(By.id("introAgreeButton"));
        agreeButton.click();
        driver.switchTo().defaultContent();
    }
}
