import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.sql.Timestamp;

public class TelerikForumTests {
    public static final String telerikForumUrl = "https://stage-forum.telerikacademy.com";
    private WebDriver driver;

    private static final String LOG_IN_BUTTON_XPATH = "//button[@class='widget-button btn btn-primary btn-small login-button btn-icon-text']";
    private static final String EMAIL_INPUT_XPATH = "//input[@name='Email']";
    private static final String EMAIL_INPUT_USERNAME = "ralka_17@abv.bg";
    private static final String PASS_INPUT_XPATH = "//input[@name='Password']";
    private static final String PASS_INPUT_PASSWORD = "testP@ss";
    private static final String SIGN_IN_BUTTON = "//button[@id='next']";
    private static final String LOGGED_IN_USER_XPATH = "//img[@title='Ralka 17']";
    private static final String BUTTON_CREATE_TOPIC_XPATH = "//button[@id='create-topic']/span";
    private static final String TITLE_INPUT_XPATH = "//input[@aria-label='Type title, or paste a link here']";
    private static final String TOPIC_NAME = "Little star";
    private static final String EXPECTED_TOPIC = "little-star";
    private static final String CONTENT_INPUT_XPATH = "//textarea[@class='d-editor-input ember-text-area ember-view']";
    private static final String TOPIC_CONTENT = "Lorem ipsum dolor sit";
    private static final String BUTTON_CREATE_XPATH = "//button[@class='btn btn-icon-text btn-primary create ember-view']";
    private static final String BUTTON_AVATAR_XPATH = "//img[@class='avatar']";
    private static final String LINK_ACTIVE_USER_XPATH = "//a[@class='widget-link user-activity-link']";
    private static final String LINK_LOGOUT_XPATH = "//li[@class='logout read']";

    @BeforeClass
    public static void classInit() {
        System.setProperty("webdriver.chrome.driver",
                "D:\\Rali\\TelerikAcademy\\Installs\\WebDrivers\\chromedriver.exe");
    }

    @Before
    public void testInit() {
        driver = new ChromeDriver();
        driver.get(telerikForumUrl);
    }

    @After
    public void testCleanup(){
        logOut();
        driver.close();
    }

    @Test
    public void logIn_when_validCredentialsArePassed(){
        logIn();
        WebElement loggedInUser = driver.findElement(By.xpath(LOGGED_IN_USER_XPATH));
        Assert.assertTrue("There is no such user. ", loggedInUser.isDisplayed());
    }

    @Test
    public void createTopic_when_logIn(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        logIn();
        clickButton(BUTTON_CREATE_TOPIC_XPATH);
        waiting(800);
        sendTextToInputField(TITLE_INPUT_XPATH,TOPIC_NAME+timestamp);
        waiting(900);
        sendTextToInputField(CONTENT_INPUT_XPATH, TOPIC_CONTENT+timestamp);
        waiting(800);
        clickButton(BUTTON_CREATE_XPATH);
        waiting(1000);

        Assert.assertTrue("The name is not found", driver.getCurrentUrl().contains(EXPECTED_TOPIC));
    }

    public void clickButton(String elementLocation) {
        WebElement element = driver.findElement(By.xpath(elementLocation));
        element.click();
    }

    public void sendTextToInputField(String elementLocation, String text) {
        WebElement element = driver.findElement(By.xpath(elementLocation));
        element.click();
        element.sendKeys(text);
    }

    private void waiting(long timeoutMilliseconds) {
        try {
            Thread.sleep(timeoutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void logIn(){
        clickButton(LOG_IN_BUTTON_XPATH);
        sendTextToInputField(EMAIL_INPUT_XPATH, EMAIL_INPUT_USERNAME);
        sendTextToInputField(PASS_INPUT_XPATH, PASS_INPUT_PASSWORD);
        clickButton(SIGN_IN_BUTTON);
    }

    public void logOut(){
        clickButton(BUTTON_AVATAR_XPATH);
        clickButton(LINK_ACTIVE_USER_XPATH);
        clickButton(LINK_LOGOUT_XPATH);
    }
}
