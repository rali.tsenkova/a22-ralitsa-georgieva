import Pages.GoogleHomePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;



public class GoogleTestsWithPageObject {
    public static final String googleSearchUrl = "https://google.com";
    private WebDriver driver;




    @BeforeClass
    public static void classInit(){
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void testInit(){
        driver = new ChromeDriver();
        driver.get(googleSearchUrl);
        agreeWithConsent(driver);
    }

    @After
    public void testCleanup(){
        driver.close();
    }

    @Test
    public void googleHomePageNavigated_When_GoogleOpened(){
        String expectedHomePageUrl = "https://www.google.com/";
        Assert.assertEquals("The page was not navigated", driver.getCurrentUrl(), expectedHomePageUrl);
    }

    @Test
    public void googleSearchResultsPageNavigated_When_SearchIsPerformed(){
        GoogleHomePage homePage = new GoogleHomePage(driver);
        waiting(5000);
        //agreeWithConsent();
        WebElement searchTextBox = driver.findElement(By.xpath("//input[@name='q']"));
        searchTextBox.sendKeys("Telerik Academy Alpha");
        waiting(1000);
        WebElement suggestionSection = driver.findElement(By.className("UUbT9"));
        WebElement searchButton = suggestionSection.findElement(By.name("btnK"));
        searchButton.click();

        homePage.assertResultPageNavigated();
    }

    private void waiting(long timeoutMilliseconds) {
        try {
            Thread.sleep(timeoutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void agreeWithConsent(WebDriver driver) {
        WebElement iFrame = driver.findElement(By.xpath("//div[@id='cnsw']//iframe"));
        driver.switchTo().frame(iFrame);
        WebElement agreeButton = driver.findElement(By.id("introAgreeButton"));
        agreeButton.click();
        driver.switchTo().defaultContent();
    }

    public void agreeWithConsent() {
        WebElement iFrame = driver.findElement(By.xpath("//div[@id='cnsw']//iframe"));
        driver.switchTo().frame(iFrame);
        WebElement agreeButton = driver.findElement(By.id("introAgreeButton"));
        agreeButton.click();
        driver.switchTo().defaultContent();
    }
}
