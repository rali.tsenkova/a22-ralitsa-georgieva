import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class GoogleTests {
    public static final String googleSearchUrl = "https://google.com";
    private WebDriver driver;

    @BeforeClass
    public static void classInit(){
//        System.setProperty("webdriver.chrome.driver",
//                "D:\\Rali\\TelerikAcademy\\Installs\\WebDrivers\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver",
                "D:\\Rali\\TelerikAcademy\\Installs\\WebDrivers\\geckodriver.exe");
    }

    @Before
    public void testInit(){
//        driver = new ChromeDriver();
        driver = new FirefoxDriver();
    }

    @After
    public void testCleanup(){
        driver.close();
    }

    @Test
    public void googleHomePageNavigated_When_GoogleOpened(){
        driver.get(googleSearchUrl);

        agreeWithConsent(driver);

        String expectedHomePageUrl = "https://www.google.com/";
        Assert.assertEquals("The page was not navigated", driver.getCurrentUrl(), expectedHomePageUrl);
    }

    @Test
    public void googleSearchResultsPageNavigated_When_SearchIsPerformed(){
        driver.get(googleSearchUrl);

        agreeWithConsent(driver);

//        WebElement searchTextBox=driver.findElement(By.name("q"));
        WebElement searchTextBox=driver.findElement(By.xpath("//input[@name='q']"));
        searchTextBox.sendKeys("Telerik Academy Alpha");      // + Keys.ENTER);

        WebElement suggestionSection = driver.findElement(By.className("UUbT9"));
        WebElement searchButton = suggestionSection.findElement(By.name("btnK"));
        waiting(1000);

//        searchButton.click();
        new Actions(driver).moveToElement(searchButton).click().perform();
        waiting(1000);

        // Assert page navigated
        Assert.assertTrue("Page was not navigated. Actual url: " + driver.getCurrentUrl() +
                        ". Expected url: " + "https://www.google.com/search",
                        driver.getCurrentUrl().contains("https://www.google.com/search"));
    }

    private void waiting(long timeoutMilliseconds) {
        try {
            Thread.sleep(timeoutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void agreeWithConsent(WebDriver driver) {
        WebElement iFrame = driver.findElement(By.xpath("//div[@id='cnsw']//iframe"));
        driver.switchTo().frame(iFrame);
        WebElement agreeButton = driver.findElement(By.id("introAgreeButton"));
        agreeButton.click();
        driver.switchTo().defaultContent();
    }
}
