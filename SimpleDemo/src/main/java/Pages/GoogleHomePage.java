package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class GoogleHomePage {
    private WebDriver driver;

    public GoogleHomePage(WebDriver webDriver){
        driver = webDriver;
    }

    // Element maps
    // Here I have problem with .findElement
    // Page Actions


    // Assertions
    public void assertResultPageNavigated(){
        Assert.assertTrue("Page was not navigated. Actual url: " + driver.getCurrentUrl() +
                        ". Expected url: " + "https://www.google.com/search",
                driver.getCurrentUrl().contains("https://www.google.com/search"));
    }
}
